package compp.ndhadadakcompany.prjsqlitecomplet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class inscriptionEtudiantActivity extends AppCompatActivity {

    EditText Enom,Eprenom,eProgramme,Ecourriel,Ecour,EnbHeure,Edate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription_etudiant);
        Enom= (EditText)findViewById(R.id.nomEtudiant);
        Eprenom= (EditText)findViewById(R.id.prenomEtudiant);
        eProgramme= (EditText)findViewById(R.id.cour);
        Ecourriel= (EditText)findViewById(R.id.email);

        EnbHeure= (EditText)findViewById(R.id.nbHeure);
        Edate= (EditText)findViewById(R.id.dateRdv);

    }

    public void inscrireEtudiants(View view) {


        String nom,prenom,porgrame,couriel,dates;
        int nbh;

        nom= Enom.getText().toString();
        prenom=Eprenom.getText().toString();
        porgrame= eProgramme.getText().toString();
        couriel=Ecourriel.getText().toString();
        dates=Edate.getText().toString();
        nbh= Integer.parseInt(EnbHeure.getText().toString());


        try {
            myDbAdapter adapter = new myDbAdapter(this);
            adapter.open();
            adapter.createEtudiant(nom,prenom,porgrame,couriel,nbh,dates);

            Toast.makeText(getApplicationContext(), "Compte crée", Toast.LENGTH_SHORT).show();

            finish();

        } catch (Exception e) {
            Log.e("error", e.getLocalizedMessage());
        }

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
