package compp.ndhadadakcompany.prjsqlitecomplet;

/**
 * Created by User on 2018-08-22.
 */

public class EtudiantClass {
    private String nom,prenom,porgrame,couriel,dates;
    private int nbh,id;

    public EtudiantClass(int id , String nom, String prenom, String porgrame, String couriel, int nbh, String dates) {
        this.nom = nom;
        this.prenom = prenom;
        this.porgrame = porgrame;
        this.couriel = couriel;
        this.dates = dates;
        this.nbh = nbh;
        this.id=id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPorgrame() {
        return porgrame;
    }

    public void setPorgrame(String porgrame) {
        this.porgrame = porgrame;
    }

    public String getCouriel() {
        return couriel;
    }

    public void setCouriel(String couriel) {
        this.couriel = couriel;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public int getNbh() {
        return nbh;
    }

    public void setNbh(int nbh) {
        this.nbh = nbh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
