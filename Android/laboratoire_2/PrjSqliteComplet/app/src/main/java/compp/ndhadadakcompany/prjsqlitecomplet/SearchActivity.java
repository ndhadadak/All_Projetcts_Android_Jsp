package compp.ndhadadakcompany.prjsqlitecomplet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    private EditText Snom;
    private ListView listSearch;
    public static ArrayList<EtudiantClass> listEtudiant = new ArrayList<EtudiantClass>();
    public static   ArrayList<String> affiche= new ArrayList<String>();
    public static   ArrayList<Integer> idEtudiant= new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Snom = (EditText) findViewById(R.id.nomEtudiants);
        listSearch = (ListView)findViewById(R.id.listSearchEtudiant);

        setContentView(R.layout.activity_search);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchActivity.this,
                android.R.layout.simple_list_item_1, affiche);
        listSearch.setAdapter(adapter);


        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(),updateEtudiantActivity.class);
                //  intent.putExtra("position",ll.getItemIdAtPosition(i));
                int x = (int) listSearch.getItemIdAtPosition(i);
                int id = idEtudiant.get(i);
                intent.putExtra("position",id);

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuItem MajoutEtudiant= menu.add(Menu.NONE,Menu.FIRST,0,"ajouter un etudiant");
        MenuItem MafficheEtudiant= menu.add(Menu.NONE,Menu.FIRST,1,"afficher un etudiant");
        MenuItem MmodifEtudiant= menu.add(Menu.NONE,Menu.FIRST,2,"modifier les infos de l 'etudiant");
        MenuItem Mrdvprecedent= menu.add(Menu.NONE,Menu.FIRST,3,"Afficher la liste des rendez-vous (précédent)");
        MenuItem MrdvProchain= menu.add(Menu.NONE,Menu.FIRST,4,"Ajoutez un prochain rendez-vous");

        MajoutEtudiant.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Toast.makeText(getApplicationContext(), "inscription etudiant", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), inscriptionEtudiantActivity.class);
                startActivity(intent);
                return false;
            }
        });


        MafficheEtudiant.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Toast.makeText(getApplicationContext(), "recherche etudiant", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intent);
                return false;
            }
        });

        return  true;
    }

    public void rechercherEtudiant(View view) {

        String requete =Snom.getText().toString();

        Toast.makeText(getApplicationContext(), "recherche en cours ....", Toast.LENGTH_SHORT).show();

        try {
            myDbAdapter adapter = new myDbAdapter(this);
            adapter.open();
            // listEtudiant =  adapter.getEtudiantsByName(requete);

            listEtudiant =  adapter.getAllEtudiants();

            for (int i= 0; i<listEtudiant.size();i++)
            {
                if ( listEtudiant.get(i).getNom().toString().equals(requete) )
                {
                    String etud = listEtudiant.get(i).getNom()+ "  "+ listEtudiant.get(i).getPrenom()+ "\n" +
                            listEtudiant.get(i).getCouriel().toString();
                    affiche.add(etud);
                    idEtudiant.add(listEtudiant.get(i).getId());
                }

            }

            finish();

        } catch (Exception e) {
            Log.e("error", e.getLocalizedMessage());
        }

        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
        startActivity(intent);
    }
}
