package compp.ndhadadakcompany.prjsqlitecomplet;

/**
 * Created by User on 2018-08-22.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class myDbAdapter {
    private Context context;
    private String DB_NAME = "DB_labEtudiant";
    private MyDBHelper dbHelper;
    private int DB_VERSION = 1;
    private SQLiteDatabase MySqliteDatabase;

    public myDbAdapter(Context context) {
        this.context = context;
        dbHelper = new MyDBHelper(context, DB_NAME, null, DB_VERSION);
    }

    public void open() {

        this.MySqliteDatabase = dbHelper.getWritableDatabase();
    }

    public void createEtudiant(String nom, String prenom, String programme, String couriel, int heure, String dates) {

        ContentValues cv = new ContentValues();
        cv.put("nom", nom);
        cv.put("prenom", prenom);
        cv.put("programme", programme);
        cv.put("couriel", couriel);
        cv.put("nbHeur", heure);
        cv.put("dates", dates);


        this.MySqliteDatabase.insert("etudiants", null, cv);

    }

    public void UpdateEtudiant(int id ,String nom, String prenom, String programme, String couriel, int heure, String dates) {

        ContentValues cv = new ContentValues();
        cv.put("nom", nom);
        cv.put("prenom", prenom);
        cv.put("programme", programme);
        cv.put("couriel", couriel);
        cv.put("nbHeur", heure);
        cv.put("dates", dates);



        this.MySqliteDatabase.update("etudiants", cv, "id = " + id, null);

    }

    public EtudiantClass getAccountid (int id) {
        Cursor cursor = this.MySqliteDatabase.query("etudiants", null, "id = " + id, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            EtudiantClass client = new EtudiantClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getInt(5),
                    cursor.getString(6));

            return client;
        }
        return null;
    }



    public ArrayList<EtudiantClass> getEtudiantsByName(String nom){

        ArrayList<EtudiantClass> clients = new ArrayList<>();
        Cursor cursor = this.MySqliteDatabase.query("etudiants", null, "nom = "+nom, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            do {

                EtudiantClass client = new EtudiantClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getInt(5),
                        cursor.getString(6));


                clients.add(client);

            } while (cursor.moveToNext());

        }

        return clients;
    }



    public ArrayList<EtudiantClass> getAllEtudiants(){

        ArrayList<EtudiantClass> clients = new ArrayList<>();
        Cursor cursor = this.MySqliteDatabase.query("etudiants", null, null, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            do {

                EtudiantClass client = new EtudiantClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getInt(5),
                        cursor.getString(6));


                clients.add(client);

            } while (cursor.moveToNext());

        }

        return clients;
    }

    private class MyDBHelper  extends SQLiteOpenHelper
    {
        public MyDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)

        {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            String query = "create table etudiants(" +
                    "id integer primary key autoincrement," +
                    "nom text," +
                    "prenom text," +
                    "programme text," +
                    "couriel text," +
                    "nbHeur numeric," +
                    "dates text);";


            db.execSQL(query);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            String query = "drop table if exists etudiants;";
            db.execSQL(query);
            onCreate(db);
        }


    }
}

