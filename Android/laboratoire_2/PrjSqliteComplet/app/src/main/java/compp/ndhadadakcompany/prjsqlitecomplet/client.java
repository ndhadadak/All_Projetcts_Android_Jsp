package compp.ndhadadakcompany.prjsqlitecomplet;

/**
 * Created by User on 2018-08-15.
 */

public class client {
    public int id;
    public String nom;
    public String prenom;
    public String adresse;
    public String user;
    public String pw;
    public double solde;
    public double credit;

    public client()
    {
        id=0;
        nom="";
        prenom="";
        adresse="";
        user="";
        pw="";
        solde=0;
        credit=0;
    }

    public client(int vid, String vnom, String vprenom, String vadresse, String vuser, String vpw, Double vsolde, Double vcredit)
    {
        id=vid;
        nom=vnom;
        prenom=vprenom;
        adresse=vadresse;
        user=vuser;
        pw=vpw;
        solde=vsolde;
        credit=vcredit;
    }
}
