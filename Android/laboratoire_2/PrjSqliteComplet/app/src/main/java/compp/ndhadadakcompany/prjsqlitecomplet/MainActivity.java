package compp.ndhadadakcompany.prjsqlitecomplet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuItem MajoutEtudiant= menu.add(Menu.NONE,Menu.FIRST,0,"ajouter un etudiant");
        MenuItem MafficheEtudiant= menu.add(Menu.NONE,Menu.FIRST,1,"afficher un etudiant");
        // MenuItem MmodifEtudiant= menu.add(Menu.NONE,Menu.FIRST,2,"modifier les infos de l 'etudiant");
        // MenuItem Mrdvprecedent= menu.add(Menu.NONE,Menu.FIRST,3,"Afficher la liste des rendez-vous (précédent)");
        //  MenuItem MrdvProchain= menu.add(Menu.NONE,Menu.FIRST,4,"Ajoutez un prochain rendez-vous");

        MajoutEtudiant.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Toast.makeText(MainActivity.this, "bonjour", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), inscriptionEtudiantActivity.class);
                startActivity(intent);
                return false;
            }
        });


        MafficheEtudiant.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Toast.makeText(MainActivity.this, "bonjour", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intent);
                return false;
            }
        });

        return  true;
    }
}
