package compp.ndhadadakcompany.prjsqlitecomplet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class updateEtudiantActivity extends AppCompatActivity {

    private EditText Enom,Eprenom,eProgramme,Ecourriel,Ecour,EnbHeure,Edate;
    public int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_etudiant);
        Enom= (EditText)findViewById(R.id.nomEtudiants);
        Eprenom= (EditText)findViewById(R.id.prenomEtudiants);
        eProgramme= (EditText)findViewById(R.id.cours);
        Ecourriel= (EditText)findViewById(R.id.emails);

        EnbHeure= (EditText)findViewById(R.id.nbHeures);
        Edate= (EditText)findViewById(R.id.dateRdvs);

        Bundle b = getIntent().getExtras();
        int  ss= b.getInt("position");

        ss -=1;


        String nom,prenom,programme,courriel,date;
        int heure;

        id = SearchActivity.listEtudiant.get(ss).getId();
        nom=SearchActivity.listEtudiant.get(ss).getNom();
        prenom=SearchActivity.listEtudiant.get(ss).getPrenom();
        programme=SearchActivity.listEtudiant.get(ss).getPorgrame();
        courriel=SearchActivity.listEtudiant.get(ss).getCouriel();
        heure=SearchActivity.listEtudiant.get(ss).getNbh();
        date=SearchActivity.listEtudiant.get(ss).getDates();


        Enom.setText(nom);
        Eprenom.setText(prenom);

        eProgramme.setText(programme);
        Ecourriel.setText(courriel);

        EnbHeure.setText(String.valueOf(heure));
        Edate.setText(date);







    }

    public void nextEtudiants(View view) {
    }

    public void updateEtudiants(View view) {

        String nom,prenom,porgrame,couriel,dates;
        int nbh;

        nom= Enom.getText().toString();
        prenom=Eprenom.getText().toString();
        porgrame= eProgramme.getText().toString();
        couriel=Ecourriel.getText().toString();
        dates=Edate.getText().toString();
        nbh= Integer.parseInt(EnbHeure.getText().toString());


        try {
            myDbAdapter adapter = new myDbAdapter(this);
            adapter.open();
            adapter.UpdateEtudiant(id,nom,prenom,porgrame,couriel,nbh,dates);

            Toast.makeText(getApplicationContext(), "mise a jour ok", Toast.LENGTH_SHORT).show();

            finish();

            SearchActivity.idEtudiant.clear();
            SearchActivity.affiche.clear();
            SearchActivity.listEtudiant.clear();
            SearchActivity.listEtudiant =  adapter.getAllEtudiants();

        } catch (Exception e) {
            Log.e("error", e.getLocalizedMessage());
        }
    }

    public void previousEtudiants(View view) {
    }
}
