package com.ndhadadakcompany.demologin;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class homeactivity extends AppCompatActivity {

    private ListView listfonctionnalites;
    private TextView txtnomuser;
    private ArrayList<String> ArrayListFonctions;
    private ArrayAdapter<String> ArrayAdapterFonctions;
    private String nameuser;
    private Context c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homeactivity);

        listfonctionnalites = (ListView) findViewById(R.id.listfonctions);
        txtnomuser = (TextView) findViewById(R.id.txtnom);
        initArrayList();
        initArrayAdapter();
        listfonctionnalites.setAdapter(ArrayAdapterFonctions);
        Intent i = getIntent();
        nameuser = i.getStringExtra("nameuser");
        txtnomuser.setText(nameuser);
        c  = this;
        listfonctionnalites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0)
                {
                   Intent i = new Intent(c,list_of_available_activities_activity.class);
                   c.startActivity(i);
                }else if(position == 1)
                {

                }else if(position == 2)
                {

                }else if(position == 3)
                {

                }else if(position == 4)
                {

                }else if(position == 5)
                {

                }else if(position == 6) {

                }

            }
        });
    }

    public void initArrayList()
    {
        ArrayListFonctions = new ArrayList<String>();
        ArrayListFonctions.add("Liste des activités disponibles");
        ArrayListFonctions.add("Inscription à une activité disponible");
        ArrayListFonctions.add("Attribuer une note à une activité");
        ArrayListFonctions.add("Liste des Etudiants d'une activité");
        ArrayListFonctions.add("Note Moyenne d'une activité");
        ArrayListFonctions.add("Statistiques d'une activité");
        ArrayListFonctions.add("Proposer une nouvelle activité");
    }

    public void initArrayAdapter()
    {
        ArrayAdapterFonctions = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ArrayListFonctions);
    }
}
