package com.ndhadadakcompany.demologin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class loginactivity extends AppCompatActivity {

    private EditText txtuser;
    private EditText txtpw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginactivity);
        txtuser = (EditText)findViewById(R.id.textuser);
        txtpw = (EditText)findViewById(R.id.textpw);
    }

    public void loginuser(View v)
    {
        String userN = this.txtuser.getText().toString();
        String userP = this.txtpw.getText().toString();
        dbworker dbw = new dbworker(this,"login");
        dbw.execute(userN,userP);
    }

    public void openhome(String nameuser)
    {
        Intent i = new Intent(getApplicationContext(),homeactivity.class);
        i.putExtra("nameuser",nameuser);
        startActivity(i);
    }


}
