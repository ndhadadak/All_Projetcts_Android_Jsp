package com.ndhadadakcompany.demologin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.zip.InflaterOutputStream;

/**
 * Created by User on 2018-05-25.
 */

public class dbworker extends AsyncTask {

    private Context c;
    private AlertDialog alertdiag;
    private String cible ;
    public String choix;
    public String[] Arrayactivites;
    public dbworker(Context c, String choix)
    {
        this.c = c;
        this.choix = choix;
    }

    @Override
    protected void onPreExecute() {
        this.alertdiag = new AlertDialog.Builder(this.c).create();
        this.alertdiag.setTitle("Login Status");
    }

    @Override
    protected Object doInBackground(Object[] param) {
        cible = "http://192.168.0.103/application_mobile/lab1/login.php";
        //192.168.0.103
        if(choix.equals("login"))
        {
            //return loginaccount(param);
            try
            {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs,"utf-8"));
                String msg = URLEncoder.encode("user","utf-8")+"="+
                        URLEncoder.encode((String)param[0],"utf-8")+
                        "&"+URLEncoder.encode("pw","utf-8")+"="+
                        URLEncoder.encode((String)param[1],"utf-8");
                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();
                while((line = bufr.readLine()) != null)
                {
                    sbuff.append(line + "\n");
                }
                return sbuff.toString();
            }
            catch(Exception ex)
            {
                return ex.getMessage();
            }
        }
        else if(choix.equals("selectallactivities"))
        {
            return selectallactivities();
        }
        else
        {
            return "test";
        }
    }

    @Override
    protected void onPostExecute(Object o) {
    if (choix.equals("login"))
    {
        String resuser = (String)o;
        if( resuser.equals("true\n"))
        {
            this.alertdiag.setMessage("Activité home");
            this.alertdiag.show();
            openhome(resuser);
        }
        else
        {
            this.alertdiag.setMessage(resuser);
            this.alertdiag.show();
        }
    }
    else if(choix.equals("selectallactivities"))
    {
        String[] myArray = (String [])o;
        String res = "";
        for(int i=0; i<myArray.length;i++)
        {
            res += myArray[i];
        }
        this.alertdiag.setMessage(res);
        this.alertdiag.show();
    }
    }

    private String loginaccount(Object [] param)
    {
        try
        {
            URL url = new URL(cible);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");

            OutputStream outs = con.getOutputStream();
            BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs,"utf-8"));
            String msg = URLEncoder.encode("user","utf-8")+"="+
                    URLEncoder.encode((String)param[0],"utf-8")+
                    "&"+URLEncoder.encode("pw","utf-8")+"="+
                    URLEncoder.encode((String)param[1],"utf-8");
            bufw.write(msg);
            bufw.flush();
            bufw.close();
            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();
            while((line = bufr.readLine()) != null)
            {
                sbuff.append(line + "\n");
            }
            return sbuff.toString();
        }
        catch(Exception ex)
        {
            return ex.getMessage();
        }
    }

    public void openhome(String nameuser)
    {
        Intent i = new Intent(c,homeactivity.class);
        i.putExtra("nameuser",nameuser);
        c.startActivity(i);
    }

    public Object selectallactivities()
    {

        try
        {
            URL url = new URL(cible);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);

            InputStream ins = con.getInputStream();
            BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while((line = bufr.readLine()) != null)
            {
                sbuff.append(line);
                Arrayactivites = (sbuff.toString().split("-"));
            }
            return Arrayactivites;
        }
        catch (Exception ex)
        {
            return null;
        }

    }

}
