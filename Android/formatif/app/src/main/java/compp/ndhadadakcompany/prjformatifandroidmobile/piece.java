package compp.ndhadadakcompany.prjformatifandroidmobile;

/**
 * Created by User on 2018-07-27.
 */

public class piece {
    public String nom;
    public String description;
    public String emplacement;
    public String image;

    public piece()
    {
        nom = "";
        description = "";
        emplacement = "";
        image = "";
    }

    public piece(String nom, String desc, String emp, String img)
    {
        this.nom = nom;
        this.description = desc;
        this.emplacement = emp;
        this.image = img;
    }


}
