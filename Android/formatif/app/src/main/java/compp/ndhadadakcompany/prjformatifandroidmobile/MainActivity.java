package compp.ndhadadakcompany.prjformatifandroidmobile;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity{
    ArrayList<piece> listpieces;
    public String resultat ;

    //Pour Listview
    private ListView ListViewPieces;
     Button btnlouer;
    private ArrayAdapter<piece> ArrayAdapterPieces;
    private Context thiscontext;
    private ArrayList<String> Arrayteststring;
    private ArrayAdapter<String> ArrayAdaptertestString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        String adresse = "https://www.google.com/";
      String adresse = "http://192.168.2.27/projet_formatif_mobile/";

      btnlouer = findViewById(R.id.btnlocation);
      ListViewPieces = findViewById(R.id.listpieces);
      thiscontext = this;

        webAsynctask webasc = new webAsynctask(adresse);
        webasc.execute();
      //  System.out.println(listpieces[0].nom);

    }

    public class webAsynctask extends AsyncTask {

        private String url;
        private String webpage;

        public webAsynctask(String url)
        {
            this.url = url;
            webpage = "";
        }
        @Override
        protected void onPreExecute()
        {
            System.out.println("Working in background......");
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            webpage = ConnectToWeb();
            return webpage;
        }

        @Override
        protected void onPostExecute(Object o)
        {
           //  resultat = (String) o;
            System.out.println((String) o);
            String page = (String) o;

            fouillepieces(page); // initialisation tableau
            InitArrayAdapter();
            ListViewPieces.setAdapter(ArrayAdaptertestString);
            ListViewPieces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String value = "Nom : " + listpieces.get(position).nom + "\n" +
                                   "Description : " + listpieces.get(position).description + "\n" +
                                   "Emplacement : " + listpieces.get(position).emplacement;
                    Toast.makeText(getApplicationContext(),value,Toast.LENGTH_LONG).show();
                }
            });

            ListViewPieces.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getApplicationContext(),LocationActivity.class);
                    i.putExtra("selectedpiecenom",listpieces.get(position).nom);
                    i.putExtra("selectedpiecedesc",listpieces.get(position).description);
                    i.putExtra("selectedpieceemp",listpieces.get(position).emplacement);
                    startActivity(i);
                    return true;
                }
            });

        }

        private String ConnectToWeb()
        {
            String result = "";
            try
            {
                URL obj = new URL(this.url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
                httpURLConnection.setRequestMethod("GET");
                int httpResponseCode = httpURLConnection.getResponseCode();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                String inputline;
                StringBuffer  stringBuffer = new StringBuffer();
                while((inputline  = bufferedReader.readLine()) != null)
                {
                    stringBuffer.append(inputline + "\n");
                }
                bufferedReader.close();
                //System.out.println(stringBuffer.toString());
                result =  stringBuffer.toString();

            } catch(MalformedURLException e)
            {
                e.printStackTrace();
                Log.i("msgerror1", e.toString());
            } catch (IOException e) {
                result = e.toString();
                Log.i("msgerror2", e.toString());
            }
            return result;
        }

        public String getWebpage()
        {
            return this.webpage;
        }

        public void fouillepieces(String pageweb)
        {
            pageweb = pageweb.substring(pageweb.indexOf("<tr>"),pageweb.lastIndexOf("</table>"));
            System.out.println(pageweb);
            String [] products = pageweb.split("\n");
            String  pieces[] = new String[100];
            int j = 0;
            for(int i = 0; i<products.length;i++)
            {
                if(products[i].contains("<td>"))
                {
                    pieces[j] = products[i].substring(products[i].indexOf("<td>"));
                    j++;
                }
            }

             listpieces = new ArrayList<piece>();
            Arrayteststring = new ArrayList<String>();
            for(int i = 0; i<37;i=i+4)
            {
                piece composant = new piece();
                composant.nom = pieces[i].substring(pieces[i].indexOf(">")+2,pieces[i].indexOf("</td>"));
                composant.description = pieces[i+1].substring(pieces[i+1].indexOf(">")+2,pieces[i+1].indexOf("</td>"));
                composant.emplacement = pieces[i+2].substring(pieces[i+2].indexOf(">")+2,pieces[i+2].indexOf("</td>"));
                composant.image = pieces[i+3].substring(pieces[i+3].indexOf(">")+2,pieces[i+3].indexOf("</td>"));
                listpieces.add(composant);
                Arrayteststring.add(composant.nom);
            }
            //System.out.println(listpieces.get(0).nom);
        }

        public void InitArrayAdapter()
        {
            //ArrayAdapterPieces = new ArrayAdapter<piece>(thiscontext, android.R.layout.simple_list_item_1, listpieces);
            ArrayAdaptertestString = new ArrayAdapter<String>(thiscontext, android.R.layout.simple_list_item_1, Arrayteststring);
        }
    }

}
