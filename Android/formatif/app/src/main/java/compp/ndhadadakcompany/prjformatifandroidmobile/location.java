package compp.ndhadadakcompany.prjformatifandroidmobile;

/**
 * Created by User on 2018-08-02.
 */

public class location {
    public String NumEtud;
    public String NumPiece;
    public String dateloc;
    public String heureloc;
    public String dateretour;

    public location()
    {
        NumEtud = "";
        NumPiece = "";
        dateloc = "";
        heureloc = "";
        dateretour = "";
    }

    public location(String vNumetud, String vNumpiece, String vDateloc, String vHeureloc, String vDateretour)
    {
        NumEtud = vNumetud;
        NumPiece = vNumpiece;
        dateloc = vDateloc;
        heureloc = vHeureloc;
        dateretour = vDateretour;
    }
}
