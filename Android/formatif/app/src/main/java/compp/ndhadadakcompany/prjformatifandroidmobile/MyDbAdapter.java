package compp.ndhadadakcompany.prjformatifandroidmobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by User on 2018-08-01.
 */

public class MyDbAdapter {
    private Context context;
    private String DATABASE_NAME = "";
    private MyDbHelper dbhelper;
    private int DATABASE_VERSION = 1;
    private SQLiteDatabase mySQLiteDatabase;

    public MyDbAdapter(Context context) {
        this.context = context;
        dbhelper = new MyDbHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private class MyDbHelper extends SQLiteOpenHelper
    {
        public MyDbHelper(Context context, String dbname, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, dbname, factory, version);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            String query = "Create table etudiant (numetud integer primary key autoincrement, nom text, prenom text, numDA text, mps text);";
            db.execSQL(query);
            query = "Create table piece (numpiece integer primary key autoincrement, nom text, description text, emplacement text, image text),";
            db.execSQL(query);
            query = "Create table location (numloc integer primary key autoincrement, numetud integer, numpiece integer, dateloc date, heureloc text, dateretour date,"
                    + "foreign key(numetud) references etudiant(numetud), foreign key(numpiece) references piece(numpiece));";
            db.execSQL(query);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String query = "Drop table if exists etudiant";
            db.execSQL(query);
            onCreate(db);
        }
    }

    public void open()
    {
        this.mySQLiteDatabase = dbhelper.getWritableDatabase();
    }

    public void insertStudent(etudiant newEtud)
    {
        ContentValues cv = new ContentValues();
        cv.put("nom",newEtud.nom);
        cv.put("prenom",newEtud.prenom);
        cv.put("numDA",newEtud.numDa);
        cv.put("mps",newEtud.mps);
        this.mySQLiteDatabase.insert("etudiant",null,cv);
    }

    public void insertStudent(piece newPiece)
    {
        ContentValues cv = new ContentValues();
        cv.put("nom", newPiece.nom);
        cv.put("description", newPiece.description);
        cv.put("emplacement", newPiece.emplacement);
        cv.put("image",newPiece.image);
        this.mySQLiteDatabase.insert("piece",null,cv);
    }

    public ArrayList<etudiant> selectEtudiant()
    {
        ArrayList<etudiant> allstudents = new ArrayList<etudiant>();
        return allstudents;
    }


}