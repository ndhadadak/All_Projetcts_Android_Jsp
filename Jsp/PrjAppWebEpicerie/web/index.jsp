<%-- 
    Document   : index
    Created on : Jul 16, 2018, 9:12:29 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Épicerie index jsp </title>
        <link href="homecss.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" 
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous" />
    </head>
    <body>
         <div class="container">
            <div class="header"> 
                <h1> Welcome to the best Shop : MKWR'SHOP </h1>
            </div>
            
            <div class="main">
               
                    <a href="categories?param=viandes"> Viandes </a> <br/>
                    <a href="categories?param=fruits"> Fruits et Légumes </a> <br/>
                    <a href="categories?param=laitier"> Produits Laitiers </a> <br/>
                    <a href="categories?param=boulangerie"> Boulangerie  </a> <br/>                
            </div>
            
            <div class="footer"> 
               The footer part
            </div>
        </div>
    </body>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
 
</html>
