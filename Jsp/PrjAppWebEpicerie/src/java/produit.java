/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class produit {
    public String nom;
    public String description;
    public double prix;
    public String url;
    
    public produit()
    {
        nom = "";
        description = "";
        prix = 0;
        url = "";
    }
    
    public produit(String vnom, String vdescription, double vprix, String vurl)
    {
        nom = vnom;
        description = vdescription;
        prix = vprix;
        url = vurl; 
    }
}
