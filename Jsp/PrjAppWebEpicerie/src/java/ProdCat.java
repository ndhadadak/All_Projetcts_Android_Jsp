
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class ProdCat {
    
    public int idcat;
    public String nomcat;
    public String description;
    public String url;
    public ArrayList<produit> myproducts;
    private DataGateWayProduits dtgwp;
    public ProdCat()
    {
        idcat = 0;
        nomcat = "";
        description = "";
        url = "";
    }
    
    public ProdCat(String nom)
    {
        nomcat = nom;
        dtgwp = new DataGateWayProduits("jdbc:mysql://localhost:3306/mydb_epicerie_jsp","root","");
        recupererInfos(nom);
        remplirArrayProduits(idcat);
    }
    
    public void recupererInfos(String value)
    {
        ProdCat info =  dtgwp.recupInfosCategorie(value);
        idcat = info.idcat;
        description = info.description;
        url = info.url;
    }
    
    public void remplirArrayProduits(int id)
    {
        myproducts = dtgwp.recupererProdCat(id);
    }
}
