
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class DataGateWayProduits {
    public String url;
    public String user;
    public String pw;
    private Connection conn;  
    public DataGateWayProduits (String url, String user, String pw)
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
        }
        
        this.pw = pw;
        this.user = user;
        this.url = url;
        connecttodatabase(url, user, pw);
    }

    
    private void connecttodatabase(String url, String user, String pw)
    {
        try {
             conn = DriverManager.getConnection(url, user, pw);
        } catch (SQLException ex) {
           System.out.println("Connection failed");
        }
    }
    
    public ProdCat recupInfosCategorie(String NomCat)
    {
                  ProdCat info = new ProdCat();
        try {
            
            PreparedStatement stm = conn.prepareStatement("select * from categories where nomcat = ? ;");
            stm.setString(1, NomCat);
            ResultSet rep = stm.executeQuery();

            info.nomcat = NomCat;
            info.idcat = rep.getInt("idcat");
            info.description = rep.getString("desccat");
            info.url = rep.getString("url_img_cat");
        } catch (SQLException ex) {
            Logger.getLogger(DataGateWayProduits.class.getName()).log(Level.SEVERE, null, ex);
        }
        return info;
    }
    
    public ArrayList<produit> recupererProdCat(int id)
    {
        ArrayList<produit> list = new ArrayList<produit>();
        try {
            
            PreparedStatement stm = conn.prepareStatement("select * from produits where categories_idcat = ? ;");
            stm.setInt(1, id);
            ResultSet rep = stm.executeQuery();
            while(rep.next())
            {
                produit p = new produit(rep.getString("nom"),rep.getString("description"),rep.getDouble("prix"),rep.getString("url"));
                list.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataGateWayProduits.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public String test()
    {
          String res = "";
         try {
            
            PreparedStatement stm = conn.prepareStatement("select * from categories where nomcat = 'viandes' ;");
            ResultSet rep = stm.executeQuery();
            while(rep.next())
            {
                res += rep.getInt("idcat") + " ";
                res += rep.getString("nomcat") + " ";
                res += rep.getString("desccat") + " ";
                res += rep.getString("url_img_cat") + " \n";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataGateWayProduits.class.getName()).log(Level.SEVERE, null, ex);
        }
         return res;
    }
}
